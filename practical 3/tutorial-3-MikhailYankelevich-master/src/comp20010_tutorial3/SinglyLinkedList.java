/*
This is the generic version of the SinglyLinkedList

You need to change the signature from:

public class SinglyLinkedList {

to:

public class SinglyLinkedList<E> {

and update all the other cases where 'String' should be replaced with 'E'.

 */

public class SinglyLinkedList<E> {

    private static class Node<E> {
        private E data;
        private Node <E> next;
        
        public Node(E e, Node<E> n) {
            this.data = e;
            this.next = n;
        }
        public E getData() {
            // TODO
        	return this.data;
        }
        public Node<E> getNextNode() {
            // TODO
        	return this.next;
        }
        public void setNextNode(Node<E> next)
        {
        	this.next=next;
        }
    }

    private Node<E> head = null;
    private int size;
    public SinglyLinkedList() {   //constructor
    }

    public int size() {
        // TODO
    	size=0;
    	Node<E> tmp = this.head;
    	while(tmp!=null) {
    		
			if(tmp.getNextNode()!=null)
			tmp=tmp.getNextNode();
			else break;
			size++;
		}
    		
    	
    	return size;
    }
    public boolean isEmpty() {
        // TODO
    	if (this.head==null)
    	{
    		return true;
    	}
    	else 
    	return false;
    }

    public Node<E> first() {
        // TODO
    	return this.head;
    }

    public Node<E> last() {
        // TODO
    	Node<E> temp = this.head;
    	Node<E> toReturn = null;
    	while(temp.getNextNode()!=null)
    	{
    		toReturn=temp;
    		temp=temp.getNextNode();
    	}
    	return toReturn;
    }

    public void addFirst(E data) {
        // TODO
    	
    	Node<E> temp=new Node<E>(data, this.head);
    	this.head=temp;
    	
    }

    public void addLast(E data) {
        // TODO
    	Node<E> currentTail=this.head;
    	while(currentTail!=null) {
			if(currentTail.getNextNode()!=null) {
				currentTail=currentTail.getNextNode();
			}
			else break;
    	}
    	Node<E> tmp=new Node<E>(data, null);
    	currentTail.setNextNode(tmp);
    }

    public E removeFirst() {
        // TODO
    	Node<E> toReturn=this.head;
    	this.head=head.getNextNode();
    	return toReturn.getData();
    }

    public E removeLast() {
        // TODO
    	Node<E> currentTail=this.head;
    	Node<E> tmp=this.head;
    	while(currentTail!=null) {
			if(currentTail.getNextNode()!=null) {
				tmp=currentTail;
				currentTail=currentTail.getNextNode();
			}
			else break;
    	}
    	
    	tmp.setNextNode(null);
    	return currentTail.getData();
    }

    public String toString() {
    	String output = new String();
    	Node<E> tmp=this.head;
		output += "size=" + size() + "\n";
		while(tmp!=null) {
			output += "> " + tmp.getData() + "\n";
			if(tmp.getNextNode()!=null)
			tmp=tmp.getNextNode();
			else break;
		}
		return output;
    }
    
    public static void main(String [] args) {
        SinglyLinkedList<String> ll = new SinglyLinkedList<String>();
        ll.addFirst("Java");
        ll.addFirst("Swift");
        ll.addFirst("remove this");
        ll.addLast("c");
        ll.addLast("c++");
        ll.addLast("c#");
        ll.addLast("python");
        ll.addLast("delete this");
        ll.removeFirst();
        ll.removeLast();

        System.out.println(ll.toString());
    }
}